# Geospatial reference data
## Corporate list of countries and territories


To address the diversity of country and territory codes and names used in the EU institutions, the European Commission has developed a corporate code list of countries and territories.

This code list was endorsed by the Information Management Steering Board in April 2023 and it became a corporate reference data asset. The new corporate list of countries and territories replaces the Country authority table and is applicable for new IT systems in the Commission from 15 September 2023. Owners of existing IT systems are invited to align their systems when possible.

The corporate list of countries and territories provides codes and names of geospatial and geopolitical entities in all official EU languages and is the result of a combination of multiple relevant standards, created to serve the requirements and use cases of the EU Institutions services (e.g. drop down menus in IT applications, maps, charts, invitation letters and other).

The list adheres to the principle of one content type per asset and is for this reason limited to current and deprecated countries and territories. This means that it does not include geographical aggregations, such as continents and regions, nor political and/or economic country groupings and organisations.

Eurostat is the Data owner of the corporate list of countries and territories, whereas the Publications Office of the EU assumes the role of Data steward.

You can consult and download the corporate list of countries and territories on the [EU Vocabularies website](https://op.europa.eu/en/web/eu-vocabularies/dataset/-/resource?uri=http://publications.europa.eu/resource/dataset/country) where you will also find the corresponding documentation. You can also check [Frequently Asked Questions (FAQ)](https://op.europa.eu/en/web/eu-vocabularies/countries-and-territories). Technical information such as sample queries can be found here on the [Wiki space](https://code.europa.eu/eu-vocabularies/countries-and-territories/-/wikis/home).

If you have any questions, don’t hesitate contacting [the EU Vocabularies support team](mailto:OP-EU-VOCABULARIES@publications.europa.eu).